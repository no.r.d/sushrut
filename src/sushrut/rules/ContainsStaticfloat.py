"""Facilitate Containing this type of static data."""


import nord.Rule as Rule
import sys


class ContainsStaticfloat(Rule):  # pragma: no cover
    """The ContainsStaticfloat rule allows the related target in the container."""

    def apply(self):
        """Implement the apply method."""
        pass

sys.modules[__name__] = ContainsStaticfloat
