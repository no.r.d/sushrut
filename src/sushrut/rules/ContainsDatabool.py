"""Facilitate Containing this type of static data."""


import nord.Rule as Rule
import sys


class ContainsDatabool(Rule):  # pragma: no cover
    """The ContainsDatabool rule allows the related target in the container."""

    def apply(self):
        """Implement the apply method."""
        pass

sys.modules[__name__] = ContainsDatabool
