"""Enable assignment to so named nodes."""

import nord.rules.PassData as PassData
import sys


class PassDataarray(PassData):
    """PassDataarray passes to a Dataarray"""

    def apply(self):
        """Make use of the delegate if possible using the builtin introspection capability."""
        self.edge.apply_delegate(self)


sys.modules[__name__] = PassDataarray
