"""Enable assignment to so named nodes."""

import nord.rules.AssignData as AssignData
import sys


class AssignDatastr(AssignData):
    """AssignDatastr sets the value of the Datastr Node."""

    def apply(self):
        """Grab the cargo and set the node's value if possible."""
        # TODO: perform the needed type checking here.
        super().apply()

sys.modules[__name__] = AssignDatastr
