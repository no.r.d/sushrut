"""Enable assignment to so named nodes."""

import nord.rules.DataPass as DataPass
import sys


class DataarrayPass(DataPass):
    """AssignDataarray sets the value of the Dataarray Node."""

    def apply(self):
        """Grab the value and pass as cargo if possible."""
        # TODO: perform the needed type checking here.
        super().apply()

sys.modules[__name__] = DataarrayPass
