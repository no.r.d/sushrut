"""Enable assignment to so named nodes."""

import nord.rules.DataAssign as DataAssign
import sys


class DatatimeAssign(DataAssign):
    """AssignDatatime sets the value of the Datatime Node."""

    def apply(self):
        """Grab the value and pass as cargo if possible."""
        # TODO: perform the needed type checking here.
        super().apply()

sys.modules[__name__] = DatatimeAssign
