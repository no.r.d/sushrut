"""Enable assignment to so named nodes."""

import nord.rules.StaticPass as StaticPass
import sys


class StaticintPass(StaticPass):
    """AssignStaticint sets the value of the Staticint Node."""

    def apply(self):
        """Grab the value and pass as cargo if possible."""
        # TODO: perform the needed type checking here.
        super().apply()


sys.modules[__name__] = StaticintPass
