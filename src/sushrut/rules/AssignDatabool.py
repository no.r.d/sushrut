"""Enable assignment to so named nodes."""

import nord.rules.AssignData as AssignData
import sys


class AssignDatabool(AssignData):
    """AssignDatabool sets the value of the Databin Node."""

    def apply(self):
        """Grab the cargo and set the node's value if possible."""
        # TODO: perform the needed type checking here.
        super().apply()

sys.modules[__name__] = AssignDatabool
