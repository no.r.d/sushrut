"""Enable assignment to so named nodes."""

import nord.rules.DataPass as DataPass
import sys


class DatadatePass(DataPass):
    """AssignDatadate sets the value of the Datadate Node."""

    def apply(self):
        """Grab the value and pass as cargo if possible."""
        # TODO: perform the needed type checking here.
        super().apply()

sys.modules[__name__] = DatadatePass
