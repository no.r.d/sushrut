from keyword import iskeyword
import re
import nord.exceptions.RuntimeException as RuntimeException


_bad_char = re.compile(r'[^\w0-9_]')
_bad_start = re.compile(r'[0-9]')
_white_space = re.compile(r'[\s]+')


def sanitize_name(inname):
    rv = inname
    if not rv.isidentifier():
        rv.replace(' ', '_')
        rv = re.sub(_bad_char, ' ', inname)
        rv = rv.strip()
        rv = re.sub(_white_space, '_', rv)
        if re.match(_bad_start, rv[0]):
            rv = "v_" + rv
        if iskeyword(rv):
            rv = 'v_' + rv
    return rv


class DefinableObject:
    def __new__(cls, dataobject):
        doa = dict()
        if not hasattr(cls, '__DefObjAttrs'):
            setattr(cls, '__DefObjAttrs', doa)
        for edge in dataobject.get_targets('contains'):
            fname = sanitize_name(edge.target.name)
            cnt = 1
            while fname in doa:
                fname = f"{fname}_{cnt}"
                cnt += 1

            def setter(name, value):
                raise RuntimeException(f"Value Setter not defined for {edge.target}")
            if hasattr(edge.target, 'set'):
                setter = edge.target.set  # noqa: F811

            def getter(name, value):
                raise RuntimeException(f"Value Getter not defined for {edge.target}")
            if hasattr(edge.target, 'get'):
                getter = edge.target.get  # noqa: F811

            doa[fname] = {'get': getter,
                          'set': setter}

            # setattr(cls, fname, None)

        return super(DefinableObject, cls).__new__(cls)
        # return cls

    def __init__(self, dataobject):
        pass

    def __getattr__(self, name):
        if name in getattr(self, '__DefObjAttrs'):
            return getattr(self, '__DefObjAttrs')[name]['get']()
        else:
            raise AttributeError(f"No such attribute named {name}")

    def __setattr__(self, name, value):
        if name in getattr(self, '__DefObjAttrs'):
            getattr(self, '__DefObjAttrs')[name]['set'](value)
        else:
            raise AttributeError(f"No such attribute named {name}")
