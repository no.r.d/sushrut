"""Provide node class which provides variable time data."""
import nord.nodes.Data as Data
import sys


class Datatime(Data):
    """Facilitate variable time data."""

    _type = str


sys.modules[__name__] = Datatime
