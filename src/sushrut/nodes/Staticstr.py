"""Provide node class which provides static strings."""
from ._static import Static
import sys


class Staticstr(Static):
    """Facilitate static strings."""

    def __init__(self, name="Static String"):
        super().__init__(name, str)

sys.modules[__name__] = Staticstr
