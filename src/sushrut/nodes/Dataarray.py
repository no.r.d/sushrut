"""Provide node class which provides variable binary data."""
import nord.nodes.Data as Data
import sys


class Dataarray(Data):
    """Facilitate variable arrays of data."""

    _type = list

    def append_to_array(self, rule):
        """This is called by the edge.apply_delegate via object interrogation."""
        if not hasattr(self, '_list'):
            self._list = list()

        self._list.append(rule.edge.get_cargo())


sys.modules[__name__] = Dataarray
