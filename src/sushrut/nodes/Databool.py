"""Provide node class which provides variable binary data."""
import nord.nodes.Data as Data
import sys


class Databool(Data):
    """Facilitate variable binary data."""

    _type = str


sys.modules[__name__] = Databool
