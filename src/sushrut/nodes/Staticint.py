"""Provide node class which provides static integers."""
from ._static import Static
import sys


class Staticint(Static):
    """Facilitate static integers."""

    def __init__(self, name="Static Int"):
        super().__init__(name, int)

sys.modules[__name__] = Staticint
