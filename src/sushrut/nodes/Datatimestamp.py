"""Provide node class which provides variable timestamp data."""
import nord.nodes.Data as Data
import sys


class Datatimestamp(Data):
    """Facilitate variable timestamp data."""

    _type = str


sys.modules[__name__] = Datatimestamp
