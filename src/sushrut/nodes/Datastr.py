"""Provide node class which provides variable string data."""
import nord.nodes.Data as Data
import sys


class Datastr(Data):
    """Facilitate variable string data."""

    _type = str


sys.modules[__name__] = Datastr
