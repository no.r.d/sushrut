"""Provide node class which provides static floats."""
from ._static import Static
import sys


class Staticfloat(Static):
    """Facilitate static floats."""

    def __init__(self, name="Static Float"):
        super().__init__(name, float)

sys.modules[__name__] = Staticfloat
