"""Provide node class which provides static floats."""
from ._static import Static
import sys


class Staticbool(Static):
    """Facilitate static floats."""

    def __init__(self, name="Static Bool"):
        super().__init__(name, bool)

sys.modules[__name__] = Staticbool
