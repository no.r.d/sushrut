"""Provide node class which provides variable date."""
import nord.nodes.Data as Data
import sys


class Datadate(Data):
    """Facilitate variable date data."""

    _type = str


sys.modules[__name__] = Datadate
