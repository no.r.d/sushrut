"""Provide node class which provides variable fixed precision numbers."""
import nord.nodes.Data as Data
import sys


class Datafixed(Data):
    """Facilitate variable fixed precision numbers."""

    _type = str

sys.modules[__name__] = Datafixed
