"""Provide node class which provides variable string data."""
import nord.nodes.Data as Data
import sys
from sushrut.utils import DefinableObject as DefinableObject


class Dataobject(Data):
    """Facilitate variable string data."""

    _type = object

    def prep_to_execute(s, runtime, verbose):
        """
        Run this node.

        Dataobject nodes contain other nodes. When executed, nothing should really happen,
        as the contained nodes will be referenced by other edges/nodes, and execute on
        their own.
        """
        s.clear_preempted()
        super().prep_to_execute(runtime, verbose)
        if len(s.get_targets('contains')) > 0:
            for edge in s.get_targets('contains'):
                # run all the contained nodes before this one.
                # but, only if those nodes have other rules targetting them
                if (not hasattr(edge, 'followed') or not edge.followed) and\
                        len(edge.target.get_source_rules()) > 1:
                    runtime.setNextNode(edge.target)
                    edge.flag_followed()
                    s.flag_preempted()
                if isinstance(edge.target, Data):
                    edge.target.suppress_output()

    def execute(self, runtime):
        """Override Data's execute method to prevent input from being requested."""
        self.flag_exe()

    def get(self):
        """
        Assemble all the child contents into a python object.

        This seems like a really bad idea. What happens if the members are changed in graph land.
        How do we ensure that the corresponding graph objects are also changed?

        Should there be getattr/setattr wrappers put around a simulated object, such that any
        python code that interacts with the instance, is actually changing contents held
        within the graph?

        So, the returned object would recursively go through all contained elements, and
        wrap their get/set methods inside the returned object's getattr/setattr calls.

        If we did that, it might look like this code below...
        """

        self.value = DefinableObject(self)
        return self.value

    def set(self, value):
        """
        Align the passed object's fields with what is contained in this dataobject.

        Then assign the aligned values. If the passed object has un-aligned
        attributes, raise an exception.
        """
        raise Exception("Not implemented....")

    def to_python(self):
        """
        Method to convert this entity into a python compatible
        structure. (For Shoshoni to work...)
        """
        if self.value is not None:
            self.value = DefinableObject(self)
        return self.value


sys.modules[__name__] = Dataobject
