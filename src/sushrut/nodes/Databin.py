"""Provide node class which provides variable binary data."""
import nord.nodes.Data as Data
import sys


class Databin(Data):
    """Facilitate variable binary data."""

    _type = str


sys.modules[__name__] = Databin
