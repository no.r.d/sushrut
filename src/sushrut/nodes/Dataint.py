"""Provide node class which provides variable integers."""
import nord.nodes.Data as Data
import sys


class Dataint(Data):
    """Facilitate variable integer data."""

    _type = str


sys.modules[__name__] = Dataint
