"""Provide visualization for typed data."""
from visnode import VisNode
from menuItem import MenuItem
import event
from log import warn
import containermanager as cm
from globalVars import PLACEDIST


class Dataarray(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, 'dataarray', app, space="sushrut")

        self.add_delegate("append_to_array", "dataarrayappend", 'pass', 'as_tgt')

        # self.onMappedFuncs.append(self.gen_contents_menu)
        # self.append_func = VisNode(uid+".append", 'arrayappendmethod', app, space="sushrut")
        # self.append_func.hide()

    def connect_allowed(self, other, ctype, direction, delegate=None):
        """
        Do not allow connections of type pass to attach to this node directly.

        Only delegates can be passed to. However, delegates are resolved
        after graph/map load. So, in order to prevent, live-edit time
        connections, only check when called by the delegate (which means)
        that delegate will not be none.
        """
        if direction == 'as_tgt' and delegate is not None and\
                ctype == 'pass':
            if delegate.uid.split(':')[0] in ('append_to_array'):
                return True
            else:
                return False
        else:
            return True

    def gen_contents_menu(self):
        def gen_hide_cbk(itm):
            """This is called by the menu item added to the shown member's model menu."""
            def cbk(*args, **kwargs):
                itm.hide()
            return cbk

        def gen_show_append(i):

            @event.callback
            def show_append(*args, **kwargs):
                """Render the array member and update the member's model menu."""
                warn(f"-.-.-.-.- Calling Dataarray show member {i}")
                # if not hasattr(i, 'visualization'):
                #     om.add_item(self.getPos(), self.app, i)

                # args[0] is the position of the event click, relative to the
                # menu, so it needs to be converted to world coordinates.
                pos = args[0] * PLACEDIST
                pos = cm.cwc().get_relative_point(base.camera, pos)  # noqa: F821
                i.show()
                i.setPos(pos[0], pos[1], pos[2])
                i.reparentTo(cm.cwc())
                if not hasattr(i, '_dyn_hide_item'):
                    mi = MenuItem('anomaly', gen_hide_cbk(i), title="hide")
                    i.menu.add_item(mi)
                    i._dyn_hide_item = True

                    # add a callback to the visualization such that after it is
                    # removed, regenerate the contents menu.
                    def gen_remove_cbk(dobj, item):
                        """Generate the on_remove handler for the visualization."""
                        def on_remove():
                            dobj.gen_contents_menu()
                        return on_remove
                    i.onUnmappedFuncs.append(gen_remove_cbk(self, i))

                    # Also, add a callback to the visualization such that if it's name is changed,
                    # update the menuitem title.
                    def gen_lbl_change_cbk(item):
                        @event.callback
                        def on_change(newtext):
                            item._container_menu_item.set_title(f"Show {newtext}")
                        return on_change
                    i.add_label_change_callback(gen_lbl_change_cbk(i))

                self.menu.hide()

            return show_append

        self._mi_append = MenuItem("dataarrayappend",
                                   gen_show_append(self.append_func),
                                   hides_menu=None,
                                   space="sushrut",
                                   title="Append to array")
        self.menu.add_item(self._mi_append)


"""

Visual features of an array:

expandable/collapsable
automatic pagination

There probably need to be some edges that allow
append, search, insert, remove, pop, slice

or, are edges the correct way to do this?

passing to an array might append?
how would insert look?



"""
