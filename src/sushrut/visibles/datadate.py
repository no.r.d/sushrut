"""Provide visualization for typed data."""
from visnode import VisNode


class Datadate(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, 'datadate', app, space="sushrut")
