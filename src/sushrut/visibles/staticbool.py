"""Provide visualization for typed data."""
from visibles.static import Static


class Staticbool(Static):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, app, name='staticbool', space="sushrut")
