"""Provide visualization for typed data."""
from visnode import VisNode


class Dataint(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, 'dataint', app, space="sushrut")
