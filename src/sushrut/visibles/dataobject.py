"""Provide visualization for typed data."""
from visnode import VisNode
from menuItem import MenuItem
from submenu import SubMenu
import event
from log import warn
import containermanager as cm
import objectmanager as om
from visibles.static import Static
from globalVars import PLACEDIST
import json
from uuid import uuid4


def recurse_dig(val):
    if type(val) == list and len(val) <= 2:
        return recurse_dig(val[0])
    else:
        return val


class Dataobject(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, 'dataobject', app, space="sushrut")
        self.contentsMenu = SubMenu(f"container", self.menu, title="Object Contents")
        self.menu.add_item(self.contentsMenu)
        self.onMappedFuncs.append(self.gen_contents_menu)

    def show_output(self, text):
        """Show the passed output."""
        pos = self.getWorldPos()

        try:
            obj = json.loads(text)

            wrk = obj["output"]
            wrk = recurse_dig(wrk)

            pos = self.getWorldPos()
            basex = pos[0]
            width = 3.0
            height = 5.7
            pos[2] -= 1.5 * height
            for itm in wrk["records"]:
                x = itm.copy()
                del x["attributes"]

                for fn in x:
                    itm = Static(str(uuid4()), None)

                    itm.setLabelText(x[fn])

                    def gen_on_map(dd, val):
                        def on_map(*args, **kwargs):
                            dd.mapentry.set_property('value', val)
                            dd.reparentTo(cm.get_by_name(self.container))
                        return on_map
                    itm.onMappedFuncs.append(gen_on_map(itm, f"{fn}: {x[fn]}"))
                    pos[0] += width

                    om.add_item(pos, None, itm)
                    itm.setPos(pos[0], pos[1], pos[2])

                pos[0] = basex
                pos[1] += height

            print(json.dumps(wrk, indent=4))

        except Exception as e:
            raise e

    def gen_contents_menu(self):

        def gen_hide_cbk(itm):
            """This is called by the menu item added to the shown member's model menu."""
            def cbk(*args, **kwargs):
                itm.hide()
            return cbk

        def gen_cbk(i):
            """Return the callback for the given member object's menu item."""
            @event.callback
            # @funlog
            def callback(*args, **kwargs):
                """Render the object member and update the member's model menu."""
                warn(f"-.-.-.-.- Calling Dataobject show member {i}")
                # args[0] is the position of the event click, relative to the
                # menu, so it needs to be converted to world coordinates.
                pos = args[0] * PLACEDIST
                pos = cm.cwc().get_relative_point(base.camera, pos)  # noqa: F821
                i.visualization.show()
                i.visualization.setPos(pos[0], pos[1], pos[2])
                i.visualization.reparentTo(cm.cwc())
                if not hasattr(i.visualization, '_dyn_hide_item'):
                    mi = MenuItem('anomaly', gen_hide_cbk(i.visualization), title="hide")
                    i.visualization.menu.add_item(mi)
                    i.visualization._dyn_hide_item = True

                    # add a callback to the visualization such that after it is
                    # removed, regenerate the contents menu.
                    def gen_remove_cbk(dobj, item):
                        """Generate the on_remove handler for the visualization."""

                        def on_remove():
                            dobj.contentsMenu.rem_item(item._container_menu_item)
                            dobj.gen_contents_menu()
                        return on_remove
                    i.visualization.onUnmappedFuncs.append(gen_remove_cbk(self, i))

                    # Also, add a callback to the visualization such that if it's name is changed,
                    # update the menuitem title.
                    def gen_lbl_change_cbk(item):
                        @event.callback
                        def on_change(newtext):
                            item._container_menu_item.set_title(f"Show {newtext}")
                        return on_change
                    i.visualization.add_label_change_callback(gen_lbl_change_cbk(i))

                self.menu.hide()
                self.contentsMenu.hide_menu()
            return callback

        for edge in self.mapentry.get_targets('contains'):
            if not self.contentsMenu.has_item(edge.target.id):
                # We haven't "contained" this target yet
                name = edge.target.name
                # if the below is true, then the target has just been
                # added in the UI.
                if type(name) is not str:
                    name = edge.target.visualization.name
                menuitem = MenuItem(edge.target.type,
                                    gen_cbk(edge.target, ),
                                    title=f"Show {name}",
                                    space=edge.target.get_space())
                menuitem.name = edge.target.id
                self.contentsMenu.add_item(menuitem)
                edge.target._container_menu_item = menuitem
                self.add_decoration(name, edge.target.type)

    def enter(self):
        pass

    def exit(self):
        pass

    def on_connect(self, conn):
        """Called when this databaject is connected to another node."""
        warn(f"ON_CONNECT CALLED on {conn.source} --- {conn.type} ----> {conn.target}")
        if conn.type == "contains":
            conn.target.hide()
            conn.hide()
            self.gen_contents_menu()
