"""Provide visualization for typed data."""
from visnode import VisNode


class Datatimestamp(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, 'datatimestamp', app, space="sushrut")
