{
    "extensions": {
        "fregata": {
            "locations": {
                "code": [
                    "dir://../fregata/src"
                ],
                "files": [
                    "dir://../fregata/src/fregata",
                    "dir://../fregata/src"
                ],
                "path": [
                    "dir://../fregata/src",
                    "dir://../fregata/src/fregata"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src/sushrut/",
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src"
                ],
                "files": [
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src/sushrut/",
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src"
                ],
                "path": [
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src/sushrut/",
                    "dir:///home/nate/Desktop/Dev/NoRD/sushrut/src"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "dir://../wrigan/src"
                ],
                "files": [
                    "dir://../wrigan/src/wrigan",
                    "dir://../wrigan/src"
                ],
                "path": [
                    "dir://../wrigan/src",
                    "dir://../wrigan/src/wrigan"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "nord",
                "rule": "assign",
                "source": "f2bd877e-99ab-4650-a363-0717be320a7f:0cb576dd-e871-4772-b117-675dfc8a131c",
                "space": "nord",
                "target": "fad9c206-b038-4368-ad13-551fa05e5862:BINARY B",
                "to_space": "sushrut"
            },
            {
                "from_space": "nord",
                "rule": "assign",
                "source": "15671879-0efc-4be2-8158-730409c78cbd:c5414484-2b6a-4607-8335-42c5ee0ad2a1",
                "space": "nord",
                "target": "849ddb06-fff9-4077-82fd-2d0cc46f49e3:ts_jimbo",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "e44f53b6-7e27-410a-a010-be4cfbf77087:Main Object",
                "space": "nord",
                "target": "3d4ffe9b-41c4-4c1d-80cd-694f4302c272:c9e39074-d886-4174-a8c3-292b2a672013",
                "to_space": "nord"
            },
            {
                "from_space": "sushrut",
                "rule": "contains",
                "source": "e44f53b6-7e27-410a-a010-be4cfbf77087:Main Object",
                "space": "nord",
                "target": "fad9c206-b038-4368-ad13-551fa05e5862:BINARY B",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "contains",
                "source": "e44f53b6-7e27-410a-a010-be4cfbf77087:Main Object",
                "space": "nord",
                "target": "b8f2ba56-f25d-4366-b1c8-e25f025c0134:TIME A",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "contains",
                "source": "e44f53b6-7e27-410a-a010-be4cfbf77087:Main Object",
                "space": "nord",
                "target": "849ddb06-fff9-4077-82fd-2d0cc46f49e3:ts_jimbo",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "contains",
                "source": "e44f53b6-7e27-410a-a010-be4cfbf77087:Main Object",
                "space": "nord",
                "target": "9358e50d-6657-4d65-8e27-ee0fc2e610b7:static_float_bobby",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "25337b8c-dbd2-4fb5-9ce3-4a350eeeead2:Static Goober",
                "space": "nord",
                "target": "f2bd877e-99ab-4650-a363-0717be320a7f:0cb576dd-e871-4772-b117-675dfc8a131c",
                "to_space": "nord"
            },
            {
                "from_space": "sushrut",
                "rule": "assign",
                "source": "e77d9668-3c5a-4b87-b153-823abb44a9bd:Timestamp Static",
                "space": "nord",
                "target": "15671879-0efc-4be2-8158-730409c78cbd:c5414484-2b6a-4607-8335-42c5ee0ad2a1",
                "to_space": "nord"
            }
        ],
        "nodes": [
            {
                "id": "15671879-0efc-4be2-8158-730409c78cbd",
                "name": "c5414484-2b6a-4607-8335-42c5ee0ad2a1",
                "position": [
                    -6.808232307434082,
                    1.9722785949707031,
                    0.8897125124931335
                ],
                "type": "data"
            },
            {
                "id": "25337b8c-dbd2-4fb5-9ce3-4a350eeeead2",
                "name": "Static Goober",
                "position": [
                    -13.179606437683105,
                    0.4481620788574219,
                    -5.069079399108887
                ],
                "space": "sushrut",
                "type": "staticint",
                "value": "Goober"
            },
            {
                "id": "3d4ffe9b-41c4-4c1d-80cd-694f4302c272",
                "name": "c9e39074-d886-4174-a8c3-292b2a672013",
                "position": [
                    15.050482749938965,
                    -1.2749404907226562,
                    -1.6319801807403564
                ],
                "type": "data"
            },
            {
                "id": "849ddb06-fff9-4077-82fd-2d0cc46f49e3",
                "name": "ts_jimbo",
                "position": [
                    1.9351876974105835,
                    0.0,
                    5.38379430770874
                ],
                "space": "sushrut",
                "type": "datatimestamp"
            },
            {
                "id": "9358e50d-6657-4d65-8e27-ee0fc2e610b7",
                "name": "static_float_bobby",
                "position": [
                    2.059239149093628,
                    0.0,
                    -0.04962035268545151
                ],
                "space": "sushrut",
                "type": "staticfloat",
                "value": "123.3"
            },
            {
                "id": "b8f2ba56-f25d-4366-b1c8-e25f025c0134",
                "name": "TIME A",
                "position": [
                    0.5143085718154907,
                    1.8243904113769531,
                    -4.950209617614746
                ],
                "space": "sushrut",
                "type": "datatime"
            },
            {
                "id": "e44f53b6-7e27-410a-a010-be4cfbf77087",
                "name": "Main Object",
                "position": [
                    5.584067344665527,
                    0.4646186828613281,
                    2.4289438724517822
                ],
                "space": "sushrut",
                "type": "dataobject"
            },
            {
                "id": "e77d9668-3c5a-4b87-b153-823abb44a9bd",
                "name": "Timestamp Static",
                "position": [
                    -12.876447677612305,
                    0.0,
                    2.4189865589141846
                ],
                "space": "sushrut",
                "type": "staticstr",
                "value": "2019-07-22 15:12:21"
            },
            {
                "id": "f2bd877e-99ab-4650-a363-0717be320a7f",
                "name": "0cb576dd-e871-4772-b117-675dfc8a131c",
                "position": [
                    -4.349437236785889,
                    0.5213432312011719,
                    -5.026295185089111
                ],
                "type": "data"
            },
            {
                "id": "fad9c206-b038-4368-ad13-551fa05e5862",
                "name": "BINARY B",
                "position": [
                    2.1888680458068848,
                    0.7039031982421875,
                    -5.10735559463501
                ],
                "space": "sushrut",
                "type": "databin"
            }
        ]
    },
    "name": "Hangry Forked Magpie"
}