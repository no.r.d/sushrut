import pathlib
import sys


def test_sanitize_name():
    sys.path.append(str(pathlib.Path().cwd()) + '/sushrut/src/sushrut')
    import utils as utils
    assert utils.sanitize_name('!@#$%^&*(){}abc123,.') == 'abc123'
    assert utils.sanitize_name('12abc3!') == 'v_12abc3'
    assert utils.sanitize_name('__abc123__') == '__abc123__'


def test_definableobject():
    sys.path.append(str(pathlib.Path().cwd()) + '/sushrut/src/sushrut')
    import nodes.Dataobject as Do
    import utils as utils
    import nord.Edge as Edge
    import nord.nodes.Data as Data
    datao = Do()
    tgt1 = Data()
    tgt1.name = 'Larry the $$ Rich Man'
    edge = Edge()
    edge.set_nodes(datao, tgt1, 'contains')
    a = utils.DefinableObject(datao)
    a.Larry_the_Rich_Man = 199
    assert tgt1.value == 199


def test_dataobject():
    sys.path.append(str(pathlib.Path().cwd()) + '/sushrut/src/sushrut')
    import nord.Map as Map
    from nord.Runtime import Runtime
    import json
    fname = "sushrut/test/test_Dataobject.n"
    karte = Map(json.load(open(fname, 'r')))
    karte.run(Runtime(), None)
    datao = karte.nodes['e44f53b6-7e27-410a-a010-be4cfbf77087']
    rv = datao.get()
    assert rv.BINARY_B == 'Goober'
    rv.BINARY_B = 'Cheese Plate'
    assert karte.nodes['fad9c206-b038-4368-ad13-551fa05e5862'].value == 'Cheese Plate'
